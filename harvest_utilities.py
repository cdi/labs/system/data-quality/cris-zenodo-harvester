from zenodo_harvester import harvest_by_orcid


# -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
# HELPER FUNCTION
# -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_


# -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
# CORE ZENODO HARVEST EVALUATIONS
# -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

def get_stats(creator_orcid):
    """
    uses the harvest_by_orcid functionality to get downloads and views for a person
    :param creator_orcid: a single ORCID as string
    :return: unique downloads and unique views (multiple views/downloads in a one hour session are not counted)
    """
    hits = harvest_by_orcid([creator_orcid])

    unique_downloads = 0
    unique_views = 0

    list = hits[creator_orcid]
    if not list:
        print("No results found. ORCID correct?")


    for l in list:
        unique_downloads += l['stats']['unique_downloads']
        unique_views += l['stats']['unique_views']
    return unique_downloads, unique_views


# -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
# MAIN
# -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_


if __name__ == '__main__':
    # creators_orcids = ['0000-0001-7430-3694', '0000-0003-0555-4128', '0000-0002-8273-6059']
    creator_orcid = '0000-0003-0555-4128'
    unique_downloads, unique_views = get_stats(creator_orcid)
    print('unique_downloads:', unique_downloads, 'unique_views:', unique_views)
