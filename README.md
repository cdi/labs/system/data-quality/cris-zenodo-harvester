# Zenodo harvester (for CRIS data quality)

- Support script for CRIS.   
- Researchers can have their research data and meta-data in CRIS.   
- Some might use Zenodo.  

This script searches [Zenodo.org](https://zenodo.org/) with given [ORCID(s)](https://orcid.org/) and returns the Zenodo data as a JSON.

## Zenodo specifics
- [Zenodo guide for developers](https://developers.zenodo.org/?python#sets)
- [example use of the request api in the browser](https://zenodo.org/search?page=1&size=20&q=creators.orcid:%220000-0003-0555-4128%22)
- how to do requests programatically:
```python
params={'q': 'creators.orcid:"' + orcid + '"'
response = requests.get('https://zenodo.org/api/records', params)
data = response.json()
```
- Zenodo limits the amount of requests: 60 requests per minute, 2000 requests per hour (20.10.2021)


## Using this script

### Quick Start: directly
1. Execute the zendodo_harvester.py
2. in **./json-results/** you'll find a folder per orcid, which contains all zenodo entries for that ID

### Quick Start: in your own code
````python
creators_orcids = ['0000-0003-0555-4128']
hits = harvest_by_orcid(creators_orcids)
````
